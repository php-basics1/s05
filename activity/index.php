<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s05-Activity</title>
</head>
<body>
    <?php session_start(); ?>

    <?php if (isset($_SESSION['email'])): ?>
        
        <?= "Hello, " . $_SESSION['email'] . "!<br>"; ?>
        <form action="./server.php" method="POST">
            <input type="submit" name="logout" value="Logout">
        </form>
        
      <?php  else : ?>
        <form action="./server.php" method="POST">
            <h1>Login Form</h1>
            Username: <input type="text" name="username" required>
            Password: <input type="password" name="password" required>
            <input type="submit" name="login" value="Login">
        </form>
        <?php if (isset($_SESSION['login_error_message'])) {
            echo $_SESSION['login_error_message'] . "<br>";
            }
        ?>
    <?php endif; ?>

</body>
</html>